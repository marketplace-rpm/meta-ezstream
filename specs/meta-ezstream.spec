%global app                     ezstream
%global d_bin                   %{_bindir}

Name:                           meta-ezstream
Version:                        1.0.0
Release:                        8%{?dist}
Summary:                        META-package for install and configure Ezstream
License:                        GPLv3

Source10:                       %{app}.local.xml
Source20:                       app.%{app}.sh
Source21:                       app.%{app}.playlist.sh

Requires:                       ezstream screen

%description
META-package for install and configure Ezstream.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{_sysconfdir}/%{app}.local.xml
%{__install} -Dp -m 0755 %{SOURCE20} \
  %{buildroot}%{d_bin}/app.%{app}.sh
%{__install} -Dp -m 0755 %{SOURCE21} \
  %{buildroot}%{d_bin}/app.%{app}.playlist.sh


%files
%config %{_sysconfdir}/%{app}.local.xml
%{d_bin}/app.%{app}.sh
%{d_bin}/app.%{app}.playlist.sh


%changelog
* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-8
- UPD: Shell scripts.

* Wed Jul 31 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-7
- UPD: SPEC-file.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-6
- UPD: build script.
- UPD: SPEC-file.

* Sun Mar 31 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-5
- UPD: "run.ezstream.playlist.sh".

* Sun Mar 31 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-4
- UPD: "run.ezstream.playlist.sh".

* Sun Mar 31 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-3
- UPD: "run.ezstream.playlist.sh".

* Sun Mar 31 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- UPD: "run.ezstream.playlist.sh".

* Sun Mar 31 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
